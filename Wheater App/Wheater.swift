//
//  Wheater.swift
//  Wheater App
//
//  Created by Yıldırımhan Atçıoğlu on 23.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import Foundation


struct WheaterData:Codable {
    var currently:Currently
    struct Currently:Codable {
        var summary:String
        var apparentTemperature:Double
    }
    
    
    
}
