//
//  ViewController.swift
//  Wheater App
//
//  Created by Yıldırımhan Atçıoğlu on 23.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate {
    var manager = CLLocationManager()
    @IBOutlet weak var city: UILabel!
    
    @IBOutlet weak var hava: UILabel!
    @IBOutlet weak var sicaklik: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization() //info.plist izni gerekir
        manager.startUpdatingLocation()
        
        
        
       
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         let location = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        manager.stopUpdatingLocation()
        let currentLocation = location.latitude.description + "," + location.longitude.description
 
        let myUrl = "https://api.darksky.net/forecast/6d4fcb62eae668381806b43321794e64/" + currentLocation + "?lang=tr"
        let url = URL(string:myUrl)!
        let myData = try! Data(contentsOf:url)
        let jsonDecoder = JSONDecoder()
        
        
        let result = try? jsonDecoder.decode(WheaterData.self, from: myData)
        if let havaDurumu = result?.currently
        {
            let celcius = Int(round((havaDurumu.apparentTemperature-30)*5/9))
            let selsius = String(celcius)+"°C"
            
            city.text = "Kozan"
            hava.text = havaDurumu.summary
            sicaklik.text = selsius
            
        }
    }
}

